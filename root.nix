{ hostname }:
{ lib, pkgs, ... }:
let
  # Import home manager channel s.t. we can use it standalone.
  home-manager = (import ./npins).home-manager;
  home-manager-cmd = ((import home-manager) { inherit pkgs; }).home-manager;
in
{
  imports = [
    { _module.args.hostname = hostname; }
    (./. + "/${hostname}")
  ];

  nix = {
    settings.auto-optimise-store = true;

    # Makes commands default to the same Nixpkgs, config, overlays and NixOS configuration
    nixPath = [
      "nixpkgs=${pkgs.path}"
      "nixos-config=${toString (./. + "/${hostname}.nix")}"
      "nixpkgs-overlays=${toString ./nixpkgs/overlays.nix}"
      "home-manager=${home-manager.outPath}"
    ];
  };

  # Use the Nixpkgs config and overlays from the local files for this NixOS build
  nixpkgs = {
    config = import ./nixpkgs/config.nix;
    overlays = import ./nixpkgs/overlays.nix;
  };

  environment = {
    systemPackages = [
      pkgs.npins
      home-manager-cmd
    ];
    variables.NIXPKGS_CONFIG = lib.mkForce (toString ./nixpkgs/config.nix);
    extraSetup = ''
      rm --force $out/bin/nix-channel
    '';
  };

  # This option is broken when set false, prevent people from setting it to false
  # And we implement the important bit above ourselves
  nix.channel.enable = true;
}
