{ pkgs, ... }:
{
  programs.sway.enable = true;

  services.greetd =
    let
      swaycmd = pkgs.writeShellScript "start sway" ''
        # Session
        export XDG_SESSION_TYPE=wayland
        export XDG_SESSION_DESKTOP=sway
        export XDG_CURRENT_DESKTOP=sway
        
        # Wayland stuff
        export MOZ_ENABLE_WAYLAND=1
        export QT_QPA_PLATFORM=wayland
        export SDL_VIDEODRIVER=wayland
        export _JAVA_AWT_WM_NONREPARENTING=1

        exec systemd-cat -t sway sway
      '';
    in
    {
      enable = true;
      settings = {
        terminal.vt = "next";
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --cmd ${swaycmd}";
          user = "greeter";
        };
      };
    };
}
