let
  hostname = "copperhouse";
in
{
  imports = [ (import ./root.nix { inherit hostname; }) ];
}
