{ lib, stdenv, fetchFromGitLab, kernel, linuxHeaders, pahole }:

stdenv.mkDerivation (finalAttrs: {
  pname = "tuxedo-drivers-${kernel.version}";
  version = "4.5.1";

  src = fetchFromGitLab {
    owner = "tuxedocomputers";
    repo = "development/packages/tuxedo-drivers";
    rev = "v${finalAttrs.version}";
    hash = "sha256-w8LfVKtRymFk3Ix/iRq5s1W7pLF9oVqTnRfQWzN2s7k=";
  };

  buildInputs = [
    pahole
    linuxHeaders
  ];

  makeFlags = [ "KDIR=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build" ];

  installPhase = ''
    runHook preInstall

    mkdir -p "$out/lib/modules/${kernel.modDirVersion}"

    find . -name '*.ko' | xargs -I% mv % $out/lib/modules/${kernel.modDirVersion}

    runHook postInstall
  '';

  meta = {
    broken = stdenv.isAarch64 || (lib.versionOlder kernel.version "5.5");
    description = "Keyboard and hardware I/O driver for TUXEDO Computers laptops";
    homepage = "https://github.com/tuxedocomputers/tuxedo-keyboard/";
    license = lib.licenses.gpl3Plus;
    longDescription = ''
      This driver provides support for Fn keys, brightness/color/mode for most TUXEDO
      keyboards (except white backlight-only models).

      Can be used with the "hardware.tuxedo-keyboard" NixOS module.
    '';
    maintainers = [ lib.maintainers.blanky0230 ];
    platforms = lib.platforms.linux;
  };
})
