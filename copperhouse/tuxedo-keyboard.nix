{ config, pkgs, ... }:
let
  tuxedo-keyboard = config.boot.kernelPackages.callPackage ./tuxedo-drivers.nix { };
in
{
  boot.kernelModules = [ "tuxedo_keyboard" ];
  boot.extraModulePackages = [ tuxedo-keyboard ];
  services.udev.packages = [
    (
      let
        name = "99-systemd-fix.rules";
      in
      pkgs.writeTextFile {
        inherit name;
        destination = "/etc/udev/rules.d/${name}";
        text = ''
          # Workaround for a systemd bug, that is causing a boot delay, when there are too many kbd_backlight devices.
          SUBSYSTEM=="leds", KERNEL=="*kbd_backlight*", TAG-="systemd"
          SUBSYSTEM=="leds", KERNEL=="*kbd_backlight", TAG+="systemd"
          SUBSYSTEM=="leds", KERNEL=="*kbd_backlight_1", TAG+="systemd"
          SUBSYSTEM=="leds", KERNEL=="*kbd_backlight_2", TAG+="systemd"
          SUBSYSTEM=="leds", KERNEL=="*kbd_backlight_3", TAG+="systemd"
        '';
      }
    )
  ];
}

