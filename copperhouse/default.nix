{ pkgs, hostname, ... }:
let
  benEdKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPswE+u4DhLXBqJ4lEgsfG0ApjcgzmqC/Iv6Jnj7wku1";
  borgkey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDy+wyUYSjJcp3GzdEGHypPVphgyp66612jjgdQT5Na+";
  user = "ben";
in
{
  imports = [
    ./hardware-configuration.nix
    ../common/boot.nix
    ../common/pipewire.nix
    ../common/sway.nix

    ./tuxedo-keyboard.nix
    (import ../common/tpm2.nix user)
  ];

  networking.hostName = hostname;
  networking.networkmanager.enable = true;

  time.timeZone = "Europe/London";

  i18n.defaultLocale = "en_GB.UTF-8";
  i18n.supportedLocales = [
    "en_GB.UTF-8/UTF-8"
    "en_US.UTF-8/UTF-8"
  ];

  security.rtkit.enable = true;

  users.users.${user} = {
    isNormalUser = true;
    extraGroups = [ "networkmanager" "wheel" "docker" "video" ];
    openssh.authorizedKeys.keys = [ benEdKey ];
  };

  environment.systemPackages = with pkgs; [
    foot
    vim
    tmux
    curl
    pavucontrol
  ];

  programs = {
    light = {
      enable = true;
      brightnessKeys.enable = true;
    };
    steam.enable = true;
  };

  services = {
    blueman.enable = true;
    openssh.enable = true;
    upower.enable = true;
  };

  system.stateVersion = "22.05";

  services.borgbackup = {
    repos.lamorna = {
      path = "/backup/lamorna";
      authorizedKeys = [ borgkey ];
    };
  };

  services.zerotierone = {
    enable = true;
    joinNetworks = [
      "159924d6301da7c6"
    ];
  };

  services.logind = {
    lidSwitch = "suspend";
    extraConfig = ''
      IdleAction=suspend
      IdleActionSec=600
    '';
  };

  systemd.services."systemd-backlight@".enable = false;
  systemd.services.keyboard-backlight = {
    before = [ "multi-user.target" ];
    script = "${pkgs.brightnessctl}/bin/brightnessctl -d 'rgb:kbd_backlight*' set 10";
    serviceConfig = {
      Type = "oneshot";
      RemainAfterExit = true;
    };
    wantedBy = [ "multi-user.target" ];
  };

  virtualisation.docker.enable = true;
}
